<?php

use App\Http\Controllers\MainController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1', 'middleware' => ['cors']], function () {
    Route::get('prepareEvents', [MainController::class, 'prepareEvents']);
    Route::get('events', [MainController::class, 'events']);
    Route::get('agenda/{id}', [MainController::class, 'agenda']);
    Route::get('agendaG/{id}', [MainController::class, 'agendaGrouped']);
    Route::post('/login', [UserController::class, 'login']);
    Route::post('/register', [UserController::class, 'store']);
    Route::post('contact', [MainController::class, 'contact']);
    Route::get('checkEmail', [UserController::class, 'checkEmail']);
    Route::post('updateEvent',[UserController::class,'updateEvent']);
    Route::get("attended/{email}",[UserController::class,'attended']);

});
