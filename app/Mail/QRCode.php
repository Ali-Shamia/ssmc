<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Address;

class QRCode extends Mailable
{
    use Queueable, SerializesModels;
    protected $user, $filename;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $filename)
    {
        $this->user = $user;
        $this->filename = $filename;
    }


    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content('email', null, null, null,
            [
                "qrcode" => asset("public/images/" . $this->filename . ".png")
            ]);
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments()
    {
        return [];
    }
}
