<?php

namespace App\Http\Controllers;

use App\Helper\_EmailHelper;
use App\Helper\_MessageHelper;
use App\Http\Resources\AuthResource;
use App\Http\Resources\BaseResource;
use App\Http\Resources\UserResource;
use App\Models\Attend;
use App\Models\Event;
use App\Models\User;
use App\Services\Interfaces\IBooking;
use App\Services\Interfaces\IUser;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $user, $book;

    /**
     * UserController constructor.
     * @param IUser $user
     * @param IBooking $book
     */
    public function __construct(IUser $user, IBooking $book)
    {
        $this->user = $user;
        $this->book = $book;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return BaseResource|JsonResponse|AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        try {
            $users = $this->user->index($request);
            return UserResource::paginable($users);
        } catch (Exception $exception) {
            return BaseResource::exception($exception);
        }
    }

    public function login(Request $request)
    {
        try {
            $user = $this->user->login($request);
            if ($user) {
                return AuthResource::create($user);
            } else {
                return BaseResource::return(_MessageHelper::NotExist);
            }
        } catch (Exception $exception) {
            return BaseResource::exception($exception);
        }
    }

    public function checkEmail()
    {
        $email = new _EmailHelper();
        return $email->sendTestEmail();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return AuthResource|JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $user = $this->user->store($request);
            if ($user) {
                $book = $this->book->book($request, $user);
                if ($book) {
                    return AuthResource::create($user);
                }
                return BaseResource::return(_MessageHelper::ErrorInRequest);
            } else {
                return BaseResource::return(_MessageHelper::_Already_Exist);
            }
        } catch (Exception $exception) {
            return BaseResource::exception($exception);
        }
    }


    function updateEvent(Request $request)
    {
        $user = \auth()->guard('api')->user();
        if ($user) {
            $this->user->updateUser($request, $user->id);
            $res = $this->book->updateBook($request, $user);
            if ($res) {
                return BaseResource::ok();
            }
            return BaseResource::return(_MessageHelper::ErrorInRequest);
        }
        return BaseResource::return(_MessageHelper::NotExist);

    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return UserResource|JsonResponse
     */
    public function show($id)
    {
        $user = $this->user->getById($id);
        if ($user) {
            return UserResource::create($user);
        } else {
            return BaseResource::return(_MessageHelper::NotExist, 400);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return UserResource|JsonResponse
     */
    public function updateUser(Request $request, $id)
    {
        try {
            $user = $this->user->updateUser($request, $id);
            if ($user) {
                return UserResource::create($user);
            } else {
                return BaseResource::return(_MessageHelper::NotExist);
            }
        } catch (Exception $ex) {
            return BaseResource::exception($ex);
        }
    }

    public function attended(Request $request, $email)
    {
        $event = Event::query()->where('event_date', '=', date('Y-m-d'))->first();
        $user = User::query()->where('email', $email)->first();
        if ($event && $user) {
            $attend = Attend::query()->where([
                'event_id' => $event->id,
                'user_id' => $user->id
            ])->first();

            if ($attend) {
                return BaseResource::return(_MessageHelper::_Already_Exist, 409);
            }
            $attend = Attend::query()->create([
                'event_id' => $event->id,
                'user_id' => $user->id
            ]);
            if ($attend) {
                return BaseResource::ok();
            }
        }
        return BaseResource::return(_MessageHelper::NotExist, 400);
    }

    public function refreshToken(Request $request)
    {
        try {

            if (Auth::guard()->check()) {
                $user = $this->user->getById(Auth::guard()->id());
                if ($user) {
                    return UserResource::create($user);
                }
            }
        } catch (Exception $ex) {
            return BaseResource::exception($ex);
        }
        return BaseResource::return(_MessageHelper::NotExist);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return UserResource|JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $user = $this->user->update($request, $id);
            if ($user) {
                return UserResource::create($user);
            } else {
                return BaseResource::return(_MessageHelper::NotExist);
            }
        } catch (Exception $ex) {
            return BaseResource::exception($ex);
        }
    }

    public function completeUserInfo(Request $request)
    {
        dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return BaseResource|JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->user->delete($id);
            return BaseResource::ok();
        } catch (Exception $ex) {
            return BaseResource::exception($ex);
        }
    }
}
