<?php

namespace App\Http\Controllers;

use App\Helper\_MessageHelper;
use App\Http\Resources\EventResource;
use App\Services\Interfaces\IAgenda;
use App\Services\Interfaces\IContact;
use App\Services\Interfaces\IEvent;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Exception;
use Illuminate\Http\Request;
use App\Http\Resources\BaseResource;
use Spatie\IcalendarGenerator\Components\Calendar;
use Spatie\IcalendarGenerator\Components\Event;
use Spatie\IcalendarGenerator\Components\Timezone;


class MainController extends Controller
{
    private $event, $agenda, $contact;

    public function __construct(IEvent $event, IAgenda $agenda, IContact $contact)
    {
        $this->event = $event;
        $this->agenda = $agenda;
        $this->contact = $contact;
    }

    function index(Request $request)
    {
        return view('welcome');
    }

    function prepareEvents(Request $request)
    {
        $res = $this->event->prepareEvents($request);
        return BaseResource::collection($res);
    }

    function events(Request $request)
    {
        $res = $this->event->index($request);

        return EventResource::collection($res);
    }

    function agenda(Request $request, $day)
    {
        $res = $this->event->getAgendaPerDay($request, $day);
        return BaseResource::collection($res);
    }

    function agendaGrouped(Request $request, $day)
    {
        $res = $this->event->getAgendaPerDayGrouped($request, $day);
        return BaseResource::collection($res);
    }

    function contact(Request $request)
    {
        try {
            $res = $this->contact->store($request);
            if ($res) {
                return BaseResource::ok();
            } else {
                return BaseResource::return(_MessageHelper::ErrorInRequest);
            }
        } catch (Exception $exception) {
            return BaseResource::exception($exception);
        }
    }

    function addToCalender(Request $request)
    {
        $calendar = Calendar::create('SSMC Education Week 2022');
        $timezone = Timezone::create('Asia/Dubai');
        $calendar = $calendar->timezone($timezone);
        try {
            $event = Event::create()
                ->name("SSMC Education Week 2022")
                ->description('Education Week 2022')
                ->alertMinutesBefore(5, 'SSMC Education Week 2022')
                ->createdAt(new DateTime(Carbon::now()))
                ->startsAt(new DateTime('12/12/2022 08:00:00', new DateTimeZone('Asia/Dubai')))
                ->endsAt(new DateTime('12/12/2022 17:00:00', new DateTimeZone('Asia/Dubai')))
                ->repeatOn([
                    new DateTime('12/13/2022 08:00:00', new DateTimeZone('Asia/Dubai')),
                    new DateTime('12/13/2022 08:00:00', new DateTimeZone('Asia/Dubai')),
                    new DateTime('12/14/2022 08:00:00', new DateTimeZone('Asia/Dubai')),
                    new DateTime('12/15/2022 08:00:00', new DateTimeZone('Asia/Dubai')),
                ]);
            $calendar->event([$event]);
            return response($calendar->get(), 200, [
                'Content-Type' => 'text/calendar; charset=utf-8',
                'Content-Disposition' => 'attachment; filename="ssmc-education-week.ics"',
            ]);
        } catch (Exception $e) {
            return response("Error in request", 200);
        }
    }
}
