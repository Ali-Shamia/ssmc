<?php

namespace App\Http\Resources;


use App\Models\Booking;
use Illuminate\Support\Facades\Auth;

class EventResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'event_date' => $this->event_date,
            'event_start_time' => $this->event_start_time,
            'event_end_time' => $this->event_end_time,
            'orders' => $this->orders,
            'attend' => $this->checkAttended($this->id)
        ];
    }

    function checkAttended($id)
    {
        $check = Auth::guard("api")->check();
        if ($check) {
            $user = auth()->guard('api')->user();
            $check = Booking::query()->where([
                'user_id' => $user->id,
                'event_id' => $id
            ])->first();
            return $check ? true : false;
        }
        return false;
    }
}
