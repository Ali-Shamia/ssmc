<?php

namespace App\Providers;

use App\Services\Facades\FAgenda;
use App\Services\Facades\FBase;
use App\Services\Facades\FBooking;
use App\Services\Facades\FContact;
use App\Services\Facades\FEvent;
use App\Services\Facades\FUser;
use App\Services\Interfaces\IAgenda;
use App\Services\Interfaces\IBase;
use App\Services\Interfaces\IBooking;
use App\Services\Interfaces\IContact;
use App\Services\Interfaces\IEvent;
use App\Services\Interfaces\IUser;
use Illuminate\Support\ServiceProvider;

class FacadeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(IBase::class, FBase::class);
        $this->app->singleton(IUser::class, FUser::class);
        $this->app->singleton(IEvent::class, FEvent::class);
        $this->app->singleton(IBooking::class, FBooking::class);
        $this->app->singleton(IAgenda::class, FAgenda::class);
        $this->app->singleton(IContact::class, FContact::class);


    }
}
