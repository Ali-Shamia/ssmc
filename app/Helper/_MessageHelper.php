<?php


namespace App\Helper;


class _MessageHelper
{
    const _Already_Exist = "The Record is already exist";
    const _Fields_Validation = "Check the fields";
    const NotExist = "The record with this details not exist";
    const ErrorInRequest = "Error in request";
}
