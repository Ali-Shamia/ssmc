<?php


namespace App\Helper;


use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPQRCode\QRcode;

class _EmailHelper
{
    public function __construct()
    {

    }

    public function sendVerification($user)
    {
        try {
            $mail = new PHPMailer();
            // SMTP configurations
            $mail->isSMTP();
            $mail->Host = 'smtp.dreamhost.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'info@ssmc-education-week.com';
            $mail->Password = 'AM8nshqT!p@y6R.';
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            $mail->Port = 465;

            $mail->setFrom('info@ssmc-education-week.com', 'SSMC Education Week');
            $mail->Sender = "info@ssmc-education-week.com";
            $mail->ContentType = "text/html;charset=UTF-8\r\n";
            $mail->Priority = 3;
            $mail->addCustomHeader("MIME-Version: 1.0\r\n");
            $mail->addCustomHeader("X-Mailer: PHP'" . phpversion() . "'\r\n");
            $mail->addAddress($user->email, $user->first_name);
            $mail->addReplyTo('info@ssmc-education-week.com', 'SSMC Education Week');
            $mail->Subject = 'SSMC Education week';

            $mail->isHTML(true);
            $filename = $user->id;
            QRcode::png($user->email, public_path("images/" . $filename . ".png"), 'L', 4, 2);
            // Email body content
            $mail->Body = view('email', ['qrcode' => asset("public/images/" . $filename . ".png")])->render();
            // Send email
            if ($mail->send()) {
                return true;
            }
        } catch (Exception $e) {
            Log::error($e);
        }
        return false;
    }

    public function sendResetPassword($user)
    {
//        $token = Str::random(64);
//        DB::table(config('auth.passwords.users.table'))->insert([
//            'email' => $user->email,
//            'token' => $token
//        ]);
//        Mail::to($user->email)->send(new ResetPasswordEmail($user));
        return true;
    }

    public function sendEmail($user)
    {
        return true;
    }

    public function sendTestEmail()
    {
        try {


            $mail = new PHPMailer;
            $mail->SMTPDebug = true;
            // SMTP configurations
            $mail->isSMTP();
            $mail->Host = 'smtp.dreamhost.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'info@ssmc-education-week.com';
            $mail->Password = 'AM8nshqT!p@y6R.';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;
            $mail->Sender = "info@ssmc-education-week.com";
            $mail->Priority = 3;
            $mail->ContentType = "text/html;charset=UTF-8\r\n";

            $mail->setFrom('info@ssmc-education-week.com', 'SSMC Education Week');

            $mail->addCustomHeader("MIME-Version: 1.0\r\n");
            $mail->addCustomHeader("X-Mailer: PHP'" . phpversion() . "'\r\n");
            $mail->addAddress("alishka_shamia1@hotmail.com", "Ali");
            $mail->addReplyTo('info@ssmc-education-week.com', 'SSMC Education Week');
            $mail->Subject = 'SSMC Education week';

            $mail->isHTML(true);
            $filename = strtotime(date("Y-m-d H:i:s"));
            QRcode::png("ali.shamia@medcon-me.com", public_path("images/" . $filename . ".png"), 'L', 4, 2);

            // Email body content
            $mail->Body = view('email', ['qrcode' => asset("public/images/" . $filename . ".png")])->render();
            // Send email
            if ($mail->send()) {
                return true;
            }
        } catch (Exception $e) {
        }
        return false;

    }


}
