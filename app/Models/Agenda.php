<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'brief', 'speaker', 'agenda_time', 'grouped', 'orders'
    ];

    public function Booking()
    {
        return $this->hasMany(Booking::class, 'agenda_id');
    }

    public function EventAgenda()
    {
        return $this->hasMany(EventAgenda::class, 'agenda_id');
    }

}
