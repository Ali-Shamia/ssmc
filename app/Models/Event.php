<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'title',
        'event_date', 'event_start_time',
        'event_end_time', 'orders'
    ];

    public function Booking()
    {
        return $this->hasMany(Booking::class, 'event_id');
    }


    public function EventAgenda()
    {
        return $this->hasMany(EventAgenda::class, 'event_id');
    }

}
