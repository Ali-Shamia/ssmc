<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attend extends Model
{
    use HasFactory;

    protected $fillable=["event_id","user_id"];


    public function Event()
    {
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
