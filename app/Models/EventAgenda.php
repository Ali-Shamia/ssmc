<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventAgenda extends Model
{
    use HasFactory;

    public function Event()
    {
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function Agenda()
    {
        return $this->belongsTo(Agenda::class, 'agenda_id');
    }
}
