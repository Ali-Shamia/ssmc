<?php


namespace App\Services\Interfaces;


use Illuminate\Http\Request;

interface IEvent extends IBase
{
    public function prepareEvents(Request $request);
    public function getAgendaPerDay(Request $request,$id);
    public function getAgendaPerDayGrouped(Request $request,$id);


}
