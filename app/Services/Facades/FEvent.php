<?php


namespace App\Services\Facades;


use App\Helper\_RuleHelper;
use App\Models\Event;
use App\Models\EventAgenda;
use App\Services\Interfaces\IEvent;
use Illuminate\Http\Request;

class FEvent extends FBase implements IEvent
{

    public function __construct()
    {
        $this->model = Event::class;
        $this->orderBy = "orders";
        $this->orderByType="asc";
        $this->rules = [
            'title' => _RuleHelper::_Rule_Require,
            'event_date' => _RuleHelper::_Rule_Require,
            'event_start_time' => _RuleHelper::_Rule_Require,
            'event_end_time' => _RuleHelper::_Rule_Require,
        ];

        $this->columns = ['title', 'event_date', 'event_start_time', 'event_end_time'];
        $this->allColumns = ['title', 'event_date', 'event_start_time', 'event_end_time'];
    }

    public function prepareEvents(Request $request)
    {
        $events = Event::query();
        if ($request->has('date')) {
            $events = $events->where([
                'event_date' => date('Y-m-d', strtotime($request->input('date')))
            ]);
        }
        $events = $events->orderBy('event_date', 'asc')->get();
        $res = [];
        foreach ($events as $event) {
            $res[$event->orders][] = [
                'id' => $event->id,
                'title' => $event->title,
                'event_date' => $event->event_date,
                'event_start_time' => $event->event_start_time,
                'event_end_time' => $event->event_end_time,
                'agenda' => $this->getAgenda($event->EventAgenda),
            ];
        }
        return $res;
    }

    function getGroupedAgenda($eventAgendas)
    {
        $list = [];
        if ($eventAgendas) {
            foreach ($eventAgendas as $eventAgenda) {
                $list[$eventAgenda->Agenda->orders][] = [
                    'title' => $eventAgenda->Agenda->title,
                    'brief' => $eventAgenda->Agenda->brief,
                    'speaker' => $eventAgenda->Agenda->speaker,
                    'agenda_time' => $eventAgenda->Agenda->agenda_time,
                    'colored' => $eventAgenda->Agenda->colored,
                    'orders'=>$eventAgenda->Agenda->orders
                ];
            }
        }
        return $list;
    }
    function getAgenda($eventAgendas)
    {
        $list = [];
        if ($eventAgendas) {
            foreach ($eventAgendas as $eventAgenda) {
                $list[] = [
                    'id' => $eventAgenda->Agenda->id,
                    'title' => $eventAgenda->Agenda->title,
                    'brief' => $eventAgenda->Agenda->brief,
                    'speaker' => $eventAgenda->Agenda->speaker,
                    'agenda_time' => $eventAgenda->Agenda->agenda_time,
                    'colored' => $eventAgenda->Agenda->colored,
                    'orders'=>$eventAgenda->Agenda->orders
                ];
            }
        }
        return $list;
    }

    function getAgendaPerDay(Request $request, $id)
    {
        $events = Event::query()->where('id', $id)->get();
        $res = [];
        foreach ($events as $event) {
            $res[] = [
                'id' => $event->id,
                'title' => $event->title,
                'event_date' => $event->event_date,
                'event_start_time' => $event->event_start_time,
                'event_end_time' => $event->event_end_time,
                'agenda' => $this->getAgenda($event->EventAgenda),
            ];
        }
        return $res;
    }
    function getAgendaPerDayGrouped(Request $request, $id)
    {
        $events = Event::query()->where('id', $id)->get();
        $res = [];
        foreach ($events as $event) {
            $res[] = [
                'id' => $event->id,
                'title' => $event->title,
                'event_date' => $event->event_date,
                'event_start_time' => $event->event_start_time,
                'event_end_time' => $event->event_end_time,
                'agenda' => $this->getGroupedAgenda($event->EventAgenda),
            ];
        }
        return $res;
    }
}
