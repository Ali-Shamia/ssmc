<?php


namespace App\Services\Facades;


use App\Models\Booking;
use App\Services\Interfaces\IBooking;
use App\Services\Interfaces\IEvent;
use Illuminate\Http\Request;

class FBooking extends FBase implements IBooking
{
    private $event;

    public function __construct(IEvent $event)
    {
        $this->model = Booking::class;
        $this->event = $event;
        $this->rules = [];
        $this->search = [];

    }

    public function book(Request $request, $user)
    {
        if ($request->has('days')) {
            foreach ($request->input('days') as $key => $day) {
                $check = $this->event->getById($key);
                if ($check) {
                    Booking::query()->create([
                        'event_id' => $check->id,
                        'user_id' => $user->id
                    ]);
                }
            }
            return true;
        }
        return null;
    }

    public function updateBook(Request $request, $user)
    {
        if ($request->has('days')) {
            $user->Booking()->delete();
            foreach ($request->input('days') as $key => $day) {
                $check = $this->event->getById($key);
                if ($check) {
                        Booking::query()->create([
                            'event_id' => $check->id,
                            'user_id' => $user->id
                        ]);
                    
                }
            }
            return true;
        }
        return null;
    }
}
