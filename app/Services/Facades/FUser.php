<?php


namespace App\Services\Facades;


use App\Helper\_RuleHelper;
use App\Models\User;
use App\Models\UserLanguage;
use App\Services\Interfaces\IUser;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Nette\Schema\ValidationException;

class FUser extends FBase implements IUser
{
    public function __construct()
    {
        $this->model = User::class;
        $this->search = [];
        $this->hasUnique = true;
        $this->unique = "email";
        $this->hashing = true;
        $this->encrypt = true;
        $this->hashingColumn = "password";
        $this->encryptColumn="password";
        $this->verificationEmail = true;
        $this->rules = [
            'first_name' => _RuleHelper::_Rule_Require,
            'last_name' => _RuleHelper::_Rule_Require,
            'speciality' => _RuleHelper::_Rule_Require,
            'email' => _RuleHelper::_Rule_Require . "|" . _RuleHelper::_Rule_Email,
            'password'=>_RuleHelper::_Rule_Require
        ];

        $this->columns = ['first_name', 'last_name', 'email', 'speciality','password'];
        $this->allColumns = ['first_name', 'last_name', 'email', 'speciality', 'password'];
    }

    public function updateUser(Request $request, $id)
    {
        $ex = $this->validationUpdateInfo($request);
        if (($ex instanceof ValidationException)) {
            throw new ValidationException($ex->getMessage(), $ex->getMessages());
        }
        $item = $this->getById($id);
        if ($item) {
            if (!$this->checkDuplicate($request, $id)) {
                return null;
            }
            $item->update([
                "first_name"=>$request->first_name,
                "last_name"=>$request->last_name,
                "speciality"=>$request->speciality,
            ]);
            return $item;
        }
        return null;
    }

    public function validationUpdateInfo(Request $request)
    {
        $updateRules = [
            'first_name' => _RuleHelper::_Rule_Require,
            'last_name' => _RuleHelper::_Rule_Require,
            'speciality' => _RuleHelper::_Rule_Require,
            'email' => _RuleHelper::_Rule_Require . "|" . _RuleHelper::_Rule_Email,
        ];
        try {
            $request->validate($updateRules);
            return true;
        } catch (ValidationException $exception) {
            return $exception;
        }
    }


    public function getByEmail($email)
    {
        return User::query()->where([
            'email' => $email
        ])->first();
    }

    public function login(Request $request)
    {
        try {
            $rules = [
                'email' => _RuleHelper::_Rule_Require . "|" . _RuleHelper::_Rule_Email,
                'password' => _RuleHelper::_Rule_Require
            ];
            $request->validate($rules);
            $user = $this->getByEmail($request->input('email'));
            if (!$user) {
                return null;
            }
            if (Hash::check($request->input('password'), $user->password)) {
                return $user;
            }
            return null;
        } catch (Exception $exception) {
            return null;
        }
    }

    public function updateAllUserPassword()
    {
        $users = User::all();
        foreach ($users as $user) {
            $user->update([
                'password' => Hash::make($user->email)
            ]);
        }
        return true;
    }
    public function bookSeat(Request $request)
    {
        $rules=[

        ];
    }
}
