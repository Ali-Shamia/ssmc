<?php


namespace App\Services\Facades;


use App\Helper\_RuleHelper;
use App\Models\Contact;
use App\Services\Interfaces\IContact;

class FContact extends FBase implements IContact
{
    public function __construct()
    {
        $this->model = Contact::class;
        $this->search = [];
        $this->hasUnique = false;
        $this->hashing = false;
        $this->encrypt = false;
        $this->rules = [
            'first_name' => _RuleHelper::_Rule_Require,
            'last_name' => _RuleHelper::_Rule_Require,
            'message_content' => _RuleHelper::_Rule_Require,
            'email' => _RuleHelper::_Rule_Require . "|" . _RuleHelper::_Rule_Email,
        ];

        $this->columns = ['first_name', 'last_name', 'email', 'message_content'];
        $this->allColumns = ['first_name', 'last_name', 'email', 'message_content'];
    }

}
